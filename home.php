<?php get_header(); ?>

	<?php

		$wp_query = new WP_Query();
			$wp_query->query(array('post_type' => 'home-page'));

			if ($wp_query->have_posts()){ 
				$wp_query->the_post(); 

				$home_page_background = types_render_field("home-page-header-background", array('raw'=>'true', 'url' => 'true'));
				$home_page__header_logo = types_render_field("home-page-header-logo", array('raw'=>'true', 'url' => 'true'));
				$home_page_header_slogan = types_render_field("home-page-header-slogan", array('raw'=>'true'));
				$home_page_main_content_title = types_render_field("home-page-main-content-title", array('raw'=>'true'));
				$home_page_main_content = types_render_field("home-page-main-content", array('raw'=>'true'));
			
				echo '<figure style="background-image: url('.$home_page_background.')" id="featured" class="home easing">';
					echo '<img src="'.$home_page_background.'" class="hidden">';
					echo '<figcaption class="easing">';
						echo '<header>';
							echo '<h1 class="logo featured">';
								echo '<span  class="visuallyhidden">Favorite Medium</span>';
								echo '<img src="'.$home_page__header_logo.'" alt="" role="presentation" aria-hidden="true">';
							echo '</h1>';
							echo '<h2  class="featured">'.$home_page_header_slogan.'</h2>';
						echo '</header>';
					echo '</figcaption>';
				echo '</figure>';
				echo'<section role="main" class="main-container">';
				    echo'<div class="main-wrapper">';
				      echo'<div class="inner">';
				        echo'<section id="intro" class="para">';
				          echo'<header class="h-intro">';
				            echo'<h2 >'.$home_page_main_content_title.'</h2>';
				          echo'</header>';
				          echo'<p>'.$home_page_main_content.'</p>';
				        echo'</section>';
				        echo'<section>';
				          echo'<header class="para">';
				            echo'<h3 >Together, we can partner with you to:</h3>';
				          echo'</header>';
				          echo'<figure role="presentation" aria-hidden="true" class="home-illustration para">';
				            echo'<div class="middle-section">';
				              echo'<div class="item left pop popout"><span class="icon improve"></span><span >Improve</span></div>';
				              echo'<div class="item center pop popout"><span class="icon iterate"></span><span >Iterate</span></div>';
				              echo'<div class="item right pop popout"><span class="icon disrupt"></span><span >Disrupt</span></div>';
				              echo'<div class="black-line"></div>';
				              echo'<div class="white-line"></div>';
				            echo'</div>';
				            echo'<div class="bottom-section">';
				              echo'<div  class="label">Focus on expansion</div>';
				              echo'<div  class="label">Focus on interruption</div>';
				            echo'</div>';
				            echo'<div class="background-section">';
				              echo'<div class="lines">';
				                echo'<div class="first"></div>';
				                echo'<div class="middle"></div>';
				                echo'<div class="last"></div>';
				              echo'</div>';
				            echo'</div>';
				          echo'</figure>';
				          echo '<div class="para content">';
				          
				          $illustration = types_child_posts('home-illlustration');
				          
				          foreach($illustration as $illustrations) {
				          	
				          	$illustration_title = $illustrations->fields['illustration-title'];
				          	$illustration_content = $illustrations->fields['illustration-content'];
				          	
				          	echo '<h4>'.$illustration_title.'</h4>';
				            echo '<p>'.$illustration_content.'</p>';
				          }
				            
				          echo '</div>
				        </section>';
			}
		wp_reset_query();
	?>

	
				        <section class="content">
				          <header class="para">
				            <h2  class="h1">Recent Projects</h2>
				          </header>
				          
				          <div class="work-list three">
				          	<a href="/case-study/intellian.html" class="item">
				              <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1411680026417-Intellian_thumbnail.jpg">
				                <figcaption>
				                  <h3>Intellian</h3>
				                  <h4></h4>
				                </figcaption>
				             </figure>
				           </a>
				          </div>
				        </section>
				        <?php

									$wp_query = new WP_Query();
									$wp_query->query(array('post_type' => 'home-page'));
									if ($wp_query->have_posts()){ 
										$wp_query->the_post(); 
										$community_and_events_title = types_render_field("community-and-events-title", array('raw'=>'true'));
										$community_and_events_content = types_render_field("community-and-events-content", array('raw'=>'true'));
						        echo'<section class="content">
						          <div class="para space5">
						            <header>
						              <h2  class="h1">'.$community_and_events_title.'</h2>
						            </header>
						            <p>'.$community_and_events_content.'</p>
						          </div>
						          <div class="para no-pad space2">
						            <section class="row community-row">';

						            	$communities_event = types_child_posts('community-and-event');
				          
								          foreach($communities_event as $communities_events) {
								          	
								          	$communities_events_logo = $communities_events->fields['communities-events-logo'];
								          	$communities_events_descrition = $communities_events->fields['communities-events-description'];
								          	$communities_events_link = $communities_events->fields['communities-events-link'];
							              
							              echo'<div class="columns large-6 text-center communities">
							              	<img src="'.$communities_events_logo.'">
							                <p class="space5">'.$communities_events_descrition.'</p>
							                <div class="btn-wrapper"> 
							                <a href="'.$communities_events_link.'" target="_blank" class="btn">Learn more</a>
							                </div>
							              </div>';
							             }
						            echo'</section>
						          </div>
						        </section>';
					      	}
					      	wp_reset_query();
					      ?>
				      </div>
				    </div>
				  </section>
	  
<?php get_footer(); ?>