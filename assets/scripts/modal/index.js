
/**
 * dependencies
 */

var cls = require('classes')
  , q = require('query')
  , matches = require('matches-selector')
  , evt = 'ontouchstart' in window ? 'touchend' : 'click';

/**
 * Name space for tabs
 */

var ns = 'data-js-modal';

/**
 * export `tabs`
 */

exports = module.exports = function(){
  exports.unlisten();
  document.addEventListener(evt, handleTabClick, false);
};

exports.unlisten = function(){
  document.removeEventListener(evt, handleTabClick, false);
}

/**
 * Event handler for tabs click
 */

function handleTabClick(e){
  if (! matches(e.target, '[' + ns + ']') && ! matches(e.target, '[data-btn-close]')) { return; }


  e.preventDefault();

  var link = e.target;


  var activeKey = ns + '-active'
    , key = link.getAttribute(ns)
    , selector = '[' + ns + '-item="' + key + '"]';

  var group = link.getAttribute(ns + '-group');
  if (group) {
    selector += '[' + ns + '-group="' + group + '"]';
  }

  var prevActive = q.all('[' + activeKey + ']');
  var len = prevActive.length;
  for (;len--;)
    prevActive[len].removeAttribute(activeKey);



  link.setAttribute(activeKey, true);
  q('body').classList.remove('modal-open-blog')
  q('body').classList.remove('modal-open-activities')

  if(matches(e.target, '[data-btn-close]')){
    var modals = q.all('.modal')
    , i = modals.length
    while(i--){
      modals[i].classList.add('hide');
    }

  }else{
    console.log(key)
    q('body').classList.add('modal-open-'+ key)
    q('div[data-modal-'+key+']').classList.remove('hide');

  }


  // q(selector).setAttribute(activeKey, true);

}
