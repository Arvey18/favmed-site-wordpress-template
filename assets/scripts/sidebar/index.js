
// dependencies
var matches = require('matches-selector')
  , classes = require('classes')
  , Emitter = require('emitter')
  , evt = 'ontouchstart' in window ? 'touchend' : 'click';

module.exports = function sidebar(selector, target){
  return new Sidebar(selector, target);
};

/**
 * Creates a new instance of `Sidebar`
 *
 * @param {String} selector
 * @param {Node} target
 * @api private
 */

function Sidebar(selector, target){
  this.selector = selector + ', ' + selector + ' *';
  this.target = target || document.body;
  this.cls = classes(this.target);

  this.target.addEventListener(evt, this.ontoggle.bind(this), false);
}

Emitter(Sidebar.prototype);

Sidebar.prototype.ontoggle = function(e){
  if (matches(e.target, this.selector)) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    if (this.active) this.hide();
    else this.show();
  }
};

Sidebar.prototype.show = function(){
  var cls = this.cls;
  this.emit('showing');
  this.cls.add('active');
  this.active = true;
  setTimeout(function(){
    cls.add('can-close');
  }, 300)
};

Sidebar.prototype.hide = function(){
  var cls = this.cls;
  this.emit('closing');
  this.cls.remove('active');
  this.active = false;
  setTimeout(function(){
    cls.remove('can-close');
  }, 300)
};
