var classes = require('classes'),
	ajax = require('ajax'),
  q = require('query');

var items = document.querySelectorAll('[data-js-timezone]')

var office = {
	sg: {
		hours: [8,18],
		lnglat: [1.3185848,103.8455665]
	},
	seoul: {
		hours: [9,19],
		lnglat: [37.5651,126.98955]
	},
	sf: {
		hours: [8,18],
		lnglat: [37.7577,-122.4376]		
	}
}

module.exports = timezone;


function timezone() {
	for(item in items){
		if(typeof items[item] == 'object'){
			setTime.apply(items[item],[]);
		}
	}
   return "timezone";
}

function setTime(){
	var el = this.querySelectorAll('.clock')
		, i = el.length
		, today = Math.ceil(new Date().getTime()/1000)
		, offset;
		
		while(i--){
			var cls = el[i]
				, open = cls.getAttribute('data-js-open').split(',')
				, close = cls.getAttribute('data-js-close').split(',')
				, parent = this
				, code = el[i].getAttribute('data-js-code');

			var url = "https://maps.googleapis.com/maps/api/timezone/json?location="+office[code].lnglat+"&key=AIzaSyACRIc0-3gP9nJ3ebiYErqgyB4sF0KHcj8&timestamp="+today+"&sensor=true"
			ajax.getJSON(url, function(res){
				var offset = (res.dstOffset / 60 / 60) + res.rawOffset / 60 / 60;

				setInterval(function(){
					var result = calcTime(offset, open, close)
					cls.innerHTML = result.time;
					classes(parent).add(result.code)
				},1000)

			})


		}
}

function leadZero(n){
	return n >= 10 ? n : "0" + n;
}
function calcTime(offset,open,close) {
    // create Date object for current location
    d = new Date();

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);

    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
    time1 = new Date(utc + (3600000*offset));
    time2 = new Date(utc + (3600000*offset));
    // console.log(nd)

    // return time as a string
    // return nd.toLocaleTimeString();
    time1.setHours(open[0]);
    // time1.setMinutes(open[1])
    time2.setHours(close[0])
    // time2.setMinutes(close[1])
    return {
    	time: leadZero(nd.getHours()) + ':' + leadZero(nd.getMinutes()) + ':' + leadZero(nd.getSeconds()),
    	code: nd > time1 && nd < time2 ? 'open' : 'close'
    };
    // return nd.toLocaleString();

}