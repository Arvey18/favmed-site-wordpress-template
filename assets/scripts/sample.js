$(document).ready(function(){

		var topnav;
		var head = $('#site-head')
		  , hero = $('#featured')
		  , img = $('img', hero);

		// if (img.complete) dofollow();
		// else img.onload = dofollow;
		var add = head && head.offsetHeight >= 70 ? 30 : 0;

		if(img){
			dofollow()	
		}

		window.addEventListener('resize', function(){
		  if (topnav){
		    topnav.threshold = hero && hero.offsetHeight - (head.offsetHeight+add);
		  }
		}, false);

		function dofollow(){
		  topnav = follow(head, hero && hero.offsetHeight - (head.offsetHeight+add));
		  img.onload = undefined;
		}
});
