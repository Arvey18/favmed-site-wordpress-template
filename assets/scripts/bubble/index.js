
var q = require('query')
  , cls = require('classes')
  , matches = require('matches-selector')
  , slice = Array.prototype.slice
  ;

var container = q('.bubbles');

if(container){

  container.addEventListener('click', function(e){
    var isBubble = matches(e.target, '.bubble');
    if ('A' === e.target.nodeName) return;
    if (! (isBubble || matches(e.target, '.bubble *')) ) return

    if (isBubble) toggle(e.target);
    else toggle(upTillMatch(e.target, '.bubble', container));

  }, false);

}


function toggle(el){
  var bubbles = q.all('.bubble', container);
  var i = bubbles.length;
  var clss, cel;

  while(i--){
    cel = bubbles[i];
    clss = cls(cel);
    if (cel === el) {
      clss.has('off') ? clss.remove('off') :
        clss.has('on') ? clss.add('off') :
          clss.add('on');
      ;
    } else {
      clss.has('on') ? clss.add('off') :
        clss.has('off') ? noop() : noop();
    }

  }
}

function noop(){ }

function upTillMatch(el, selector, root){
  root || (root = document.body)
  if (el === root) return null;
  if (matches(el, selector)) {
    return el;
  } else {
    return upTillMatch(el.parentNode, selector, root);
  }
}

function on(e){
  cls(this)
}