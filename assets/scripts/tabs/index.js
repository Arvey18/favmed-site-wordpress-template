
/**
 * dependencies
 */

var cls = require('classes')
  , q = require('query')
  , matches = require('matches-selector')
  , evt = 'ontouchstart' in window ? 'touchend' : 'click';

/**
 * Name space for tabs
 */

var ns = 'data-js-tab';

/**
 * export `tabs`
 */

exports = module.exports = function(){
  exports.unlisten();
  document.addEventListener(evt, handleTabClick, false);
};

exports.unlisten = function(){
  document.removeEventListener(evt, handleTabClick, false);
}

/**
 * Event handler for tabs click
 */

function handleTabClick(e){

  if ( ! matches(e.target, '[' + ns + ']')) { return; }

  e.preventDefault();

  var link = e.target;

  var activeKey = ns + '-active'
    , key = link.getAttribute(ns)
    , selector = '[' + ns + '-item="' + key + '"]';

  var group = link.getAttribute(ns + '-group');
  if (group) {
    selector += '[' + ns + '-group="' + group + '"]';
  }

  var prevActive = q.all('[' + activeKey + ']');
  var len = prevActive.length;
  for (;len--;)
    prevActive[len].removeAttribute(activeKey);

    console.log(link, activeKey)

  link.setAttribute(activeKey, true);
  q(selector).setAttribute(activeKey, true);

}
