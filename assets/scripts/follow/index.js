
/**
 * dependencies
 */

var classes = require('classes');


/**
 * exports `follow`
 */

module.exports = follow;


/**
 * toggle fixed class when element scroll
 * over threshold passed in
 *
 * @param {Element} elem
 * @param {Number} threshold
 * @param {String} cls
 * @return {Follow}
 * @api public
 */

function follow (elem, threshold, cls) {
  return new Follow(elem, threshold, cls);
};

function Follow(elem, threshold, cls) {
  this.threshold = threshold;
  this.cls = classes(elem);
  this.toggleCls = cls || 'fixed';
  this.onscroll = scroll.bind(this);
  window.addEventListener('scroll', this.onscroll, false);
}

Follow.prototype.destroy = function(){
  window.removeEventListener('scroll', this.onscroll, false);
}

Follow.prototype.update = function(){
  var cls = this.toggleCls;
  if (this.top >= this.threshold)
    this.cls.has(cls) || this.cls.add(cls);
  else
    this.cls.remove(cls);
}

function scroll(){
  var body = document.body;
  var top = body.scrollTop > 0 ? body.scrollTop : document.documentElement.scrollTop > 0 ? document.documentElement.scrollTop : 0;
  if (top !== this.top){
    this.top = top;
    this.update();
  }
}
