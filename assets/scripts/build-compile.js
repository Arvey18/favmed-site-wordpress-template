(function(){

  var compileBtn = document.getElementById('build-compile');
  var publishBtn = document.getElementById('build-publish');

  function getXhr(path, method){
    var xhr = new XMLHttpRequest();
    xhr.open(method || 'POST', path, true);
    return xhr;
  }

  compileBtn.addEventListener('click', compile, false);
  publishBtn.addEventListener('click', publish, false);

  function compile(e) {
    var btn = this;
    var oldTxt = btn.textContent;

    e.preventDefault();

    if ('compiling...' === oldTxt) return;

    var xhr = getXhr('/api/compile');
    xhr.onreadystatechange = function(){
      if (this.readyState != 4) return;
      if ('OK' === this.responseText) {
        location.reload();
        btn.textContent = oldTxt;
      }
    };
    xhr.send();
    btn.textContent = 'compiling...';
  }

  function publish(e){
    e.preventDefault();
    var btn = this;
    var oldTxt = btn.textContent;

    if ('compiling...' === oldTxt) return;

    var txt = prompt('Enter "publish" to publish to production.');

    if ('publish' !== txt) return;

    var xhr = getXhr('/api/publish');
    xhr.onreadystatechange = function(){
      if (this.readyState != 4) return;
      if ('OK' === this.responseText) {
        alert('Site will be updated in 10-30mins');
        btn.textContent = oldTxt;
      }
    };
    xhr.send();
    btn.textContent = 'compiling...';
  }

})();