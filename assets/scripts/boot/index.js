
// Sidebar
require('./sidebar')

// Tabs

require('tabs')();

// Carousel

require('carousel')();

// Top navigation

require('./top-nav');

// Contat Us form

require('contact-us');

// Fast click

// require('fastclick')(document);

require('modal')();

// Bubbles

require('bubble');

require('timezone')();

require('popout');
require('./default');
require('parallax');

