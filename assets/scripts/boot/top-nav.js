
var q = require('query')
  , follow = require('follow');

var topnav;
var head = q('#site-head')
  , hero = q('#featured')
  , img = q('img', hero);

// if (img.complete) dofollow();
// else img.onload = dofollow;
var add = head && head.offsetHeight >= 70 ? 30 : 0;

if(img){
	dofollow()	
}

window.addEventListener('resize', function(){
  if (topnav){
    topnav.threshold = hero && hero.offsetHeight - (head.offsetHeight+add);
  }
}, false);

function dofollow(){
  topnav = follow(head, hero && hero.offsetHeight - (head.offsetHeight+add));
  img.onload = undefined;
}
