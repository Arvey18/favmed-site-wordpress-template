
var q = require('query')
  , matches = require('matches-selector')
  , evt = 'ontouchstart' in window ? 'touchend' : 'click';

var sidebar = require('sidebar')('[data-js-sidebar]')
  , scrolltop = require('scroll-top')()
  , menu = q('#main-menu');

sidebar.on('showing', function(){
  // menu.setAttribute('style', 'padding-top: ' + scrolltop.value + 'px');
}).on('closing', function(){
  menu.setAttribute('style', '');
});

document.addEventListener(evt, hide, false);
document.addEventListener('click', link, false);

function hide(e){
  if (matches(e.target, '[role="main"], [role="main"] *, #site-foot, #site-foot *')) {
    sidebar.hide();
  } else if (matches(e.target, '.can-close a.close')) {
    sidebar.hide();
    e.preventDefault();
    e.stopImmediatePropagation();
  }

}

function link(e){
  if ( ! matches(e.target, '#main-menu section a')) { return; }
  sidebar.hide();
  setTimeout(function(){
    window.location = e.target.href;
  }, 300);
  e.preventDefault();
}