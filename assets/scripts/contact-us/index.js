
/**
 * dependencies
 */

var serialize = require('serialize');

/**
 * form
 */

var form = document.querySelector('form.contact');

/**
 * wire up form when it exists
 */

if (form)
  form.addEventListener('submit', submit, false);

/**
 * event listener for form submit event
 */

function submit(e){
  var xhr = getXHR();

  if (!xhr) { return }

  disbleButtons(form, true);

  var name = '\x61\x72\x65\x5f\x79\x6f\x75\x5f\x73\x75\x72\x65\x5f\x74\x6f\x5f\x67\x6f';
  var pttrn = new RegExp('('+name+'=.*)&?', 'gi');
  var data = serialize(form).replace(pttrn, name+'\x3d\u200b\x2b\u200b');

  e.preventDefault();
  xhr.open(form.method, form.action, true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function handleXhr(e){
    if (xhr.readyState === 4) {
      if (xhr.status == 204) {
        alert('Thank you!\nWe will get back to you in a short while... :)');
        form.reset();
      } else {
        alert('We are sorry!\nThere is problem submitting your message.\n\nCould you do us a favor to send your message to connect@favoritemedium.com?\nThanks alot!')
      }
      disbleButtons(form, false);
    } else { /* ignore */ }
  };
  xhr.send(data);
}

function disbleButtons(form, status){
  var buttons = form.querySelectorAll('button, [type=submit]');
  var len = buttons.length;

  while (len--) buttons[len].disabled = status;
}

/**
 * Determine XHR.
 * stolen from https://github.com/visionmedia/superagent/blob/master/superagent.js#L453-L468
 */

function getXHR() {
  if (XMLHttpRequest
    && ('file:' != location.protocol || !ActiveXObject)) {
    return new XMLHttpRequest;
  } else {
    try { return new ActiveXObject('Microsoft.XMLHTTP'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
    try { return new ActiveXObject('Msxml2.XMLHTTP'); } catch(e) {}
  }
  return false;
}