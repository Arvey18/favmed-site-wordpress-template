
/**
 * dependencies
 */

var classes = require('classes'),
  q = require('query'),
  timer,
  queue = [];


/**
 * exports `follow`
 */

module.exports = popout;


/**
 * toggle fixed class when element scroll
 * over threshold passed in
 *
 * @param {Element} elem
 * @param {Number} threshold
 * @param {String} cls
 * @return {Follow}
 * @api public
 */

function popout (elem, threshold, cls) {
  return new Popout(elem, threshold, cls);
};

function Popout(elem, threshold, cls) {
  this.threshold = threshold;
  this.cls = classes(elem);
  this.toggleCls = cls || 'fixed';
  this.onscroll = scroll.bind(this);
  scroll.apply(this);
  window.addEventListener('scroll', this.onscroll, false);
}

Popout.prototype.destroy = function(){
  window.removeEventListener('scroll', this.onscroll, false);
}


Popout.prototype.pop = function(queue){
  queue.forEach(function(el, idx){
    TweenLite.to(el,.5,{scale:1, opacity:1, delay: idx/5})
  })
}

Popout.prototype.queue = function(){
  var el = this;
  if(this.cls.has('pop')){
    queue.push(this.cls.el)
    this.cls.remove('pop')
    clearTimeout(timer)
  }

  timer = setTimeout(function(){
    el.pop(queue)
  },10)


}

function scroll(){
  var main = document.documentElement;
  var top = main.scrollTop > 0 ? main.scrollTop : document.documentElement.scrollTop > 0 ? document.documentElement.scrollTop : 0;
  var img = this.cls;
  var position = img.el.getBoundingClientRect();
  if(position.top <= (window.innerHeight - 50)){
    this.queue();
  }
}
