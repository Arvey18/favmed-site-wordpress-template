/**
 * dependencies
 */

var swipe = require('swipe')
  , classes = require('classes')
  , indexof = require('indexof');

var canvasWidth = document.body.offsetWidth;

/**
 * expose `carousels`
 */

module.exports = carousels;

/**
 *
 * @param {String} selector
 */

function carousels(interval, cls){
  // if ( ! selector) selector = '[data-js-carousel]';
  if ( ! interval) interval = 0;
  if ( ! cls) cls = 'carousel-js';

  var items = document.querySelectorAll('[data-js-carousel]')
    , len = items.length
    , carousels = []
    , width = document.body.offsetWidth;

  while (len--) carousels.push(new Carousel(items[len], interval, cls));

  window.addEventListener('resize', function(){
    canvasWidth = document.body.offsetWidth;
  });
  window.addEventListener('orientationchange', function(){
    canvasWidth = document.body.offsetWidth;
  });

  setTimeout(function refresh(){
    if (width !== canvasWidth){
      var i = carousels.length, c;
      width = canvasWidth;
      while (i--) {
        c = carousels[i];
        c.carousel.refresh();
        c.adjustChildrenWidth(width);
      }

    }
    setTimeout(refresh, 10);
  }, 10)

  return carousels;
}


function Carousel(el, interval, cls){
  var footer = document.createElement('footer')
    , nav = document.createElement('nav')
    , l = el.children[0].children.length
    , autoplay = el.getAttribute('data-js-carousel-auto') !== "0";

  while (l--) footer.appendChild(document.createElement('a'));

  footer.children[0].className = 'current';
  footer.addEventListener('click', this.show.bind(this), false);

  // navigation
  nav.appendChild(makeNav('prev'));
  nav.appendChild(makeNav('next'));
  nav.addEventListener('click', this.nav.bind(this), false);
  el.appendChild(nav);
  el.appendChild(footer);

  this.el = el;
  this.cls = classes(this.el);
  this.cls.add(cls);
  this.carousel = swipe(this.el);
  this.carousel.on('showing', this.toggleState.bind(this), false);
  classes(this.carousel.currentEl).add('current')

  if (autoplay) {
    this.carousel.duration(1000);
    this.carousel.play();
  }

  this.adjustChildrenWidth(canvasWidth);

  setTimeout(this.carousel.refresh.bind(this.carousel), 100)
}

Carousel.prototype.adjustChildrenWidth = function(width){
  var child = this.el.children[0], l;
  width = this.el.offsetWidth;
  if ( ! (child || child.chilren)) { return; }
  l = child.children.length;
  while(l--) child.children[l].setAttribute('style', 'width: ' +width+ 'px;');
};

Carousel.prototype.adjustNavPosition = function(){

};

Carousel.prototype.toggleState = function(){
  var container = this.el.lastElementChild
    , children = this.el.lastElementChild.children
    , current = this.carousel.current;

  classes(container.querySelector('a.current')).remove('current');
  classes(children[current]).add('current');
};

Carousel.prototype.show = function(e){
  var children = this.el.lastElementChild.children
    , c = this.carousel
    , idx = indexof(children, e.target);
  classes(c.currentEl).remove('current');
  c.show(idx);
  classes(c.currentEl).add('current');
};

Carousel.prototype.goto = function(e){
  var children = this.el.lastElementChild.children
    , c = this.carousel;
  classes(c.currentEl).remove('current');
  c.show(e);
  classes(c.currentEl).add('current');
};


Carousel.prototype.next = function(e){
  var c = this.carousel;
  classes(c.currentEl).remove('current');
  c.next();
  classes(c.currentEl).add('current');
}
Carousel.prototype.prev = function(e){
  var c = this.carousel;
  classes(c.currentEl).remove('current');
  c.prev();
  classes(c.currentEl).add('current');
}

Carousel.prototype.nav = function(e){
  var cls = classes(e.target)
    , c = this.carousel;
  if (cls.has('prev')) {
    if (c.isFirst()) return this.goto(c.visible -1);
    this.prev();
  } else if (cls.has('next')) {
    if (c.isLast()) return this.show(0);
    this.next()
  }
};

/**
 * helpers
 */

function makeNav(cls){
  var link = document.createElement('a');
  var icon = document.createElement('span');
  link.appendChild(icon);
  link.className = cls;
  icon.className = 'icon ' + cls;
  return link;
}