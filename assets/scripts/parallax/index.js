var x;
/**
 * dependencies
 */

var classes = require('classes'),
  q = require('query');


/**
 * exports `parallax`
 */

module.exports = parallax;


/**
 * toggle fixed class when element scroll
 * over threshold passed in
 *
 * @param {Element} elem
 * @param {Number} threshold
 * @param {String} cls
 * @return {Follow}
 * @api public
 */

var requestAnimationFrame = window.requestAnimationFrame || 
                            window.mozRequestAnimationFrame || 
                            window.webkitRequestAnimationFrame || 
                            window.msRequestAnimationFrame;

var ticking = false;

function parallax (elem, threshold, cls) {
  return new Parallax(elem, threshold, cls);
};

function Parallax(elem, threshold, cls) {
  // q('#intro').setAttribute('data-top', classes(q('#intro')).el.offsetTop);
  // setTimeout(function(){
  //   q('section[role=main]').setAttribute('data-top', q('section[role=main]').offsetTop)

  // }, 300)
  this.onscroll = scroll.bind(this);
  window.addEventListener('scroll', this.onscroll, false);
  window.addEventListener('resize', function(){
    q('section[role=main]').setAttribute('data-top', q('section[role=main]').offsetTop)      
    
  });

}

Parallax.prototype.destroy = function(){
  window.removeEventListener('scroll', this.onscroll, false);
}


function isMobile(){
   return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false;
}

function scroll(){
  if(isMobile()) return false;

  startTick();

  if(ticking){
    var body = document.body;
    var top = body.scrollTop > 0 ? body.scrollTop : document.documentElement.scrollTop > 0 ? document.documentElement.scrollTop : 0;
    // q('#featured').setAttribute('style', '-webkit-transform: translateY('+ -(top / 15) +'px) translateZ(0)');
    q('#featured').style.webkitTransform = 'translateY('+ (top / 2) +'px) translateZ(0)';
    q('#featured').style.transform       = 'translateY('+ (top / 2) +'px) translateZ(0)';

    q('.main-wrapper .inner').style.webkitTransform = 'translateY('+ (top / 5) +'px) translateZ(0)';
    q('.main-wrapper .inner').style.transform       = 'translateY('+ (top / 5) +'px) translateZ(0)';

    q('.main-wrapper').style.paddingBottom = q('#site-foot').clientHeight + 'px';
    q('.main-wrapper').style.marginBottom  = q('#site-foot').clientHeight + 100 + 'px';
    // q('.main-container').style.paddingBottom = q('#site-foot').clientHeight + 'px';

    // if(q('#featured figcaption')){
    //   q('#featured figcaption').style.webkitTransform = 'translateY('+ -(top + (top / 10))+'px) translateZ(0)';
    //   q('#featured figcaption').style.transform = 'translateY('+ -(top + (top / 10))+'px) translateZ(0)';      
    // }

    // var f = parseInt(q('section[role=main]').getAttribute('data-top'));    
    // var x = parseInt(q('#intro').getAttribute('data-top'));
    //   var addTop = 63;
    // if((x+ top/5) <= 1 ){
    //   q('section[role=main]').setAttribute('style', 'margin-top: ' + (f -(top / 10)) + 'px');
    //   q('#intro').setAttribute('style', '-webkit-transform: translateY('+ ((x+(top/5)) + addTop ) +'px) translateZ(0)');
    // }
  }
  
}

function startTick(){
  if(!ticking){
    requestAnimationFrame(update)
    ticking = true
    classes(q('body')).add('scrolling');
  }
}

function update() {
  ticking = false;
  classes(q('body')).remove('scrolling');
}

