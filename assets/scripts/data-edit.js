var xx;
(function(){

  [].slice.call(document.querySelectorAll('[data-edit-key]')).forEach(parse);

  var lang = document.documentElement.getAttribute('lang');
  var toggle = document.querySelector('#editor-toggle');
  var editor = document.querySelector('#editor');

  document.addEventListener('keyup', function(e){
    if (27 === e.keyCode) editor.classList.remove('active');
  });

  toggle.addEventListener('click', function(e){
    this.classList.toggle('active');
    document.body.classList.toggle('edit-on');
  }, false)

  editor.querySelector('span.cancel').addEventListener('click', function(e){
    editor.classList.remove('active');
  });
  editor.querySelector('form').addEventListener('submit', function(e){
    e.preventDefault();
    var key = editor.querySelector('[name=key]').value;
    var val = editor.querySelector('[name=val]').value;
    var encodedVal = encodeURIComponent(val);
    document.querySelector('[data-edit-key="'+key+'"]').innerHTML = val;

    editor.classList.remove('active');

    var xhr = new XMLHttpRequest();
    xhr.open('POST', this.action, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('lang='+lang+'&key='+key+'&val='+encodedVal);
  })

  function parse(el){
    var keys = el.getAttribute('data-edit-key').split('.');

    el.addEventListener('click', showEditor, false);
  }

  function showEditor(e){

    if (!document.body.classList.contains('edit-on')) {
      return;
    }

    e.preventDefault();
    e.stopPropagation();

    var key = this.getAttribute('data-edit-key');
    getData(key, function(data){
      var input = editor.querySelector('[name=val]');
      editor.querySelector('[name=key]').value = key;
      input.value = data.val;
      input.focus();
      editor.classList.add('active');
    });
  }

  function getData(key, next){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/api/get?key='+key+'&lang='+lang, true);
    xhr.onreadystatechange = function(e){
      if (this.readyState != 4) return;
      try {
        next(JSON.parse(this.response));
      } catch (e) {
        return alert('There are issue with the server.');
      }
    }
    xhr.send();
  }

  function findPos(el){
    var pleft = ptop = 0;

    if (el.offsetParent) {
      do {
        ptop += el.offsetTop;
        pleft += el.offsetLeft;
      } while (el = el.offsetParent)
    }
    return [pleft, ptop];
  }

})();