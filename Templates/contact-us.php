<?php 

/*
Template Name: Contact Us Template Page
*/

get_header(); ?>

	<!-- Contact Us page PHP code for showing data that came from wordpress -->
	<?php
		$wp_query = new WP_Query();
		$wp_query->query(array('post_type' => 'contact-us-page'));
		
		if ($wp_query->have_posts()){

			$wp_query-> the_post();
			$contact_us_header_bg = types_render_field("contact-us-background-header", array('raw'=>'true', 'url' => 'true'));
			$contact_us_header_title = types_render_field("contact-us-header-title", array('raw'=>'true'));
			$contact_us_header_sub_title = types_render_field("contact-us-header-sub-title", array('raw'=>'true'));
			$contact_us_main_content = types_render_field("contact-us-main-content", array('raw'=>'true'));
			$join_us_title = types_render_field("join-section-title", array('raw'=>'true'));
			$join_us_content = types_render_field("join-section-content", array('raw'=>'true'));
			$join_us_background = types_render_field("join-section-background", array('raw'=>'true'));

			//start of Header Section
			echo '<figure id="featured" style="background-image: url('.$contact_us_header_bg.');" class="contact easing">';
				echo '<img src="'.$contact_us_header_bgss.'" class="hidden">';
			  echo '<figcaption class="easing">';
			    echo '<header>';
			      echo '<h2 class="featured dashed-cap">';
			      	echo '<span class="focus">'.$contact_us_header_title.'</span>';
			      	echo '<span>'.$contact_us_header_sub_title.'</span>';
			      echo '</h2>';
			    echo '</header>';
			  echo '</figcaption>';
			echo '</figure>';
			//End of Header Section

			echo '<section role="main" class="main-container">';
		    echo '<div class="main-wrapper">';
		      echo '<div class="inner">';
		        echo '<section id="intro" class="para">';
		          echo '<header>';
		            echo '<h1 data-edit-key="contactpage.title">Contact Us</h1>';
		          echo '</header>';
		          echo '<p data-edit-key="contactpage.content">'.$contact_us_main_content.'</p>';
		          echo '<section class="work-list address three para content">';
		            echo '<figure class="item">';
		              echo '<div data-js-timezone class="timepiece pop popout sg">';
		              	echo '<span data-js-code="sg" data-js-open="8" data-js-close="18" class="clock"></span>';
		              echo '</div>';
		              echo '<figcaption>';
		                echo '<address>';
		                  echo '<div data-edit-key="contactpage.addresses.0.country" class="title">Singapore';
		                  echo '</div>';
		                  echo '<span data-edit-key="contactpage.addresses.0.address">7 Ann Siang Road #03-01';
		                  echo '<br >Singapore 069791</span>';
		                  echo '<div data-edit-key="contactpage.addresses.0.open" class="for-editor">8';
		                  echo '</div>';
		                  echo '<div data-edit-key="contactpage.addresses.0.close" class="for-editor">18';
		                  echo '</div>';
		                echo '</address>';
		              echo '</figcaption>';
		            echo '</figure>';
		            echo '<figure class="item">';
		              echo '<div data-js-timezone class="timepiece pop popout seoul">';
		              	echo '<span data-js-code="seoul" data-js-open="9" data-js-close="19" class="clock">';
		              	echo '</span>';
		              echo '</div>';
		              echo '<figcaption>
		                <address>
		                  <div data-edit-key="contactpage.addresses.1.country" class="title">Seoul
		                  </div><span data-edit-key="contactpage.addresses.1.address">#201 16, Gangnam-daero 154-Gil, 
		                  <br >Gangnam-gu, 
		                  <br >Seoul, South Korea 
		                  <br ></span>
		                  <div data-edit-key="contactpage.addresses.1.open" class="for-editor">9
		                  </div>
		                  <div data-edit-key="contactpage.addresses.1.close" class="for-editor">19
		                  </div>
		                </address>';
		            echo '</figcaption>';
		            echo '</figure>';
		            echo '<figure class="item">';
		              echo '<div data-js-timezone class="timepiece pop popout sf">';
		              	echo '<span data-js-code="sf" data-js-open="8" data-js-close="18" class="clock">';
		              	echo '</span>';
		              echo '</div>';
		            echo '<figcaption>';
		              echo '<address>';
		                echo '<div data-edit-key="contactpage.addresses.2.country" class="title">San Francisco';
		                echo '</div>';
		                echo '<span data-edit-key="contactpage.addresses.2.address">1970 Broadway, Suite 850';
		                echo '<br >Oakland, CA 94612</span>';
		                echo '<div data-edit-key="contactpage.addresses.2.open" class="for-editor">8';
		                echo '</div>';
		                echo '<div data-edit-key="contactpage.addresses.2.close" class="for-editor">18';
		                echo '</div>';
		                echo '</address>';
		              echo '</figcaption>';
		            echo '</figure>';
		          echo '</section>';
		          echo '<hr>';
		        echo '</section>';
		        echo '<section class="para space5">';
		          echo '<h3 data-edit-key="contactpage.business.header">New Business &amp; Partnerships</h3>';
		          echo '<p data-edit-key="contactpage.business.content" class="para text-center">We would love to hear from you, unless you are a robot. We would love to hear from you, unless you are a robot.  We would love to hear from you, unless you are a robot.  We would love to</p>';
		          echo '<h4 class="text-center"> <a data-edit-key="contactpage.business.email" href="mailto:connect@favoritemedium.com">connect@favoritemedium.com</a></h4>';
		          echo '<h2 data-edit-key="contactpage.business.phone">+1 (415) 484 4366</h2>';
		          echo '<div class="space5"></div>';
		        echo '</section>';
		        echo '<figure style="background-image:url('.$join_us_background.')" id="join" class="join-our-team"><img src="" class="hidden">';
		          echo '<figcaption class="easing">';
		            echo '<header>';
		              echo '<h2 data-edit-key="contactpage.join.header">'.$join_us_title.'</h2>';
		              echo '<p data-edit-key="contactpage.join.content" class="para space3">'.$join_us_content.'</p>';
		              echo '<a data-edit-key="contactpage.join.button" href="mailto:careers.14@favoritemedium.com" class="btn">Send your resume</a>';
		            echo '</header>';
		          echo '</figcaption>';
		        echo '</figure>';
		      echo '</div>';
		    echo '</div>';
		  echo '</section>';
		  }
		wp_reset_query();
	?>
<?php get_footer(); ?>