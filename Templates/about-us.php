<?php 

/*
Template Name: About Us Template Page
*/

get_header(); ?>

	<?php
		$wp_query = new WP_Query();
		$wp_query->query(array('post_type' => 'team'));
		
		if ($wp_query->have_posts()){

			$wp_query-> the_post();
			$team_header_background = types_render_field("team-header-background", array('raw'=>'true', 'url' => 'true'));
			$team_header_pre_title = types_render_field("team-header-pre-title", array('raw'=>'true'));
			$team_header_main_title = types_render_field("team-header-main-title", array('raw'=>'true'));
			$team_header_sub_title = types_render_field("team-header-sub-title", array('raw'=>'true'));
			$main_content = types_render_field("main-content", array('raw'=>'true'));
			$qoute = types_render_field("qoute", array('raw'=>'true'));
			$author = types_render_field("author-name", array('raw'=>'true'));
			$author_position = types_render_field("author-position", array('raw'=>'true'));

			echo'<figure id="featured" class="team" style="background-image: url('.$team_header_background.');">
				<img src="" class="hidden">
		      <figcaption>
		        <header class="dashed-cap">
		          <h2 class="featured para">
		          	<span  class="dash">'.$team_header_pre_title.'</span>
		          	<span  class="focus">'.$team_header_main_title.'</span>
		          	<span >'.$team_header_sub_title.'</span>
		          </h2>
		        </header>
		      </figcaption>
		    </figure>';

	echo'<section role="main" class="main-container">
      <div class="main-wrapper">
        <div class="inner">
          <section id="intro" class="para content">
            <header>
              <h1 >About Us</h1>
            </header>
            <p>'.$main_content.'</p>
  
            <blockquote>
              <p >'.$qoute.'</p>
              <footer><span >'.$author.'</span>,&nbsp;<small >'.$author_position.'</small></footer>
            </blockquote>
          </section>';
  }
	?>
          <nav class="tabs"><a href="#leadership" data-js-tab="leadership" data-js-tab-active >Leadership</a><a href="#the-team" data-js-tab="team" >The team</a>
          </nav>
          <section class="tab-contents para">
            <section id="leadership" data-js-tab-item="leadership" data-js-tab-active class="content">
              <article class="leader vcard">
                <figure><img src="/images/persons/sooyoung.jpg" alt="Sooyoung Park’s profile image" class="photo pop popout">
                  <figcaption><a href="https://twitter.com/5ooyoung"  class="url">@5ooyoung</a>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Sooyoung Park
                  </h1>
                  <h2  class="h4 title">Founder and CEO
                  </h2>
                </header>
                <input id="leader.0" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>Sooyoung has an uncanny ability to find exceptional talent. Beyond describing his work as paying the bills and deciphering contracts, as the Founder and Chief Executive Officer, Sooyoung builds relationships with internal talent and FM’s clients--essentially, he surrounds himself with motivated, creative, and curious people that he likes and finds inspiring.</p>
                  <p>Always passionate about technology, Sooyoung started programming at 12. After completing an undergraduate degree in Mathematics at Oberlin and a Masters in Computer Science at the University of Chicago, he later designed and built interactive products for many successful brands including Playboy, Cisco, Wells Fargo, MTV, and AT&amp;T Wireless. Despite all the accolades we could name, Sooyoung is proudest of the fact that he can do more pull-ups than anyone in the company.</p>
                  <label for="leader.0" class="expand-details"></label>
                </div>
              </article>
              <article class="leader vcard">
                <figure><img src="/images/persons/sean.jpg" alt="Sean Kang’s profile image" class="photo pop popout">
                  <figcaption><span >I’m a lover, not a tweeter</span>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Sean Kang
                  </h1>
                  <h2  class="h4 title">Managing Director, NE Asia
                  </h2>
                </header>
                <input id="leader.1" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>Sean began his career in consulting at PwC, but his passion for online technologies led him into game development and publishing where he spent over 12 years based in Hong Kong, the UK, and Korea, spearheading NCsoft’s online game expansion into Asia, North America, Europe, and Russia. He also worked with numerous major partners in each territory to negotiate joint venture and licensing deals and launch new online business models. At FM, Sean not only actively engages clients but also seeks out projects that challenge designers and developers.</p>
                  <p>When he’s not busy biking or looking over his fantasy basketball team, he also serves as General Manager for FM APAC. Sean has a dual degree in Industrial Management and History and Policy from Carnegie Mellon.</p>
                  <label for="leader.1" class="expand-details"></label>
                </div>
              </article>
              <article class="leader vcard">
                <figure><img src="/images/persons/brett.jpg" alt="Brett Webb’s profile image" class="photo pop popout">
                  <figcaption><a href="https://twitter.com/brettwebb"  class="url">@brettwebb</a>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Brett Webb
                  </h1>
                  <h2  class="h4 title">Vice President, FM/US
                  </h2>
                </header>
                <input id="leader.2" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>Brett takes the lead as General Manager of the Bay Area office while also working strategically with FM worldwide. Coming from a strong tech management background, Brett is used to working with far-flung teams while growing client relationships closer to home. Keyed in to the latest in development, Brett is always tinkering with something, whether learning new languages or mixing palettes, he brings a solid reputation for producing award-winning software to his new position at FM/US.</p>
                  <p>For more than 20 years, Brett has rolled up his sleeves and created innovative and compelling digital products including Emmy Award-winning work for MTV and ABC. As Hot Studio’s Executive Director of Technology, Brett led a multinational team of engineers overseeing strategic processes and implementing new technologies. Before Hot, Brett held leadership roles at Funny Garbage and worked on Second Life at Linden Lab. Brett also co-created Art Crimes, was a featured panelist at the Rock and Roll Hall of Fame, and the keynote at graffiti conferences around the world.</p>
                  <label for="leader.2" class="expand-details"></label>
                </div>
              </article>
              <article class="leader vcard">
                <figure><img src="/images/persons/kristy.jpg" alt="Kristy LaFolette’s profile image" class="photo pop popout">
                  <figcaption><a href="https://twitter.com/kristyo"  class="url">@kristyo</a>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Kristy LaFolette
                  </h1>
                  <h2  class="h4 title">Vice President of Client Relations and Operations, FM/US
                  </h2>
                </header>
                <input id="leader.3" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>She’s our own social butterfly. Kristy manages business development and cultivating client relationships as our Vice President of Client Relations and Operations, FM/US. With 20+ years of digital expertise and team building, Kristy brings to FM broad client-relations knowledge and an outstanding personal and professional network.</p>
                  <p>Kristy finds passion in people and fostering connections leading to strong and lasting partnerships. Former lives at Walmart.com, Wired, CNET and Hot Studio have given her some strong chops in not just growing sales channels but creative management, product development and marketing.</p>
                  <label for="leader.3" class="expand-details"></label>
                </div>
              </article>
              <article class="leader vcard">
                <figure><img src="/images/persons/jon.jpg" alt="Jon Siegel’s profile image" class="photo pop popout">
                  <figcaption><a href="https://twitter.com/jonsiegel"  class="url">@jonsiegel</a>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Jon Siegel
                  </h1>
                  <h2  class="h4 title">Executive Creative Director, APAC
                  </h2>
                </header>
                <input id="leader.4" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>As our Executive Creative Director for APAC, Jon is the agency detective, taking the Sam Spade approach. He asks questions and through careful observation uncovers elegant solutions to complex puzzles and surprising details that tell a brand’s story with digital grace. Jon founded the Tokyo based creative agency Pikkles in 2007, and built relationships with Fortune 500 companies across government, fashion, IT, and financial industries in Asia Pacific and North America. He drove digital media and branding initiatives for the World Bank, the United Nations University, Microsoft, NEC, and Electronic Arts.</p>
                  <p>Jon is an active player in the Singapore creative community as the organizer and host of popular speaking event, PechaKucha Night Singapore, where he seeks out and introduces engaging local creative and entrepreneurial talent. His love lies not only in his passion for digital media, but also in his personal explorative and experimental photography. Jon holds a BFA in communication design from the Massachusetts College of Art.</p>
                  <label for="leader.4" class="expand-details"></label>
                </div>
              </article>
              <article class="leader vcard">
                <figure><img src="/images/persons/andy.jpg" alt="Andy Pratt’s profile image" class="photo pop popout">
                  <figcaption><a href="https://twitter.com/andyprattdesign"  class="url">@andyprattdesign</a>
                  </figcaption>
                </figure>
                <header>
                  <h1  class="h3 fn">Andy Pratt
                  </h1>
                  <h2  class="h4 title">Executive Creative Director, FM/US
                  </h2>
                </header>
                <input id="leader.5" type="checkbox" name="checkbox" value="value" class="show-details">
                <div  class="info-wrapper">
                  <p>Andy’s deep love of creating smart and fun visual elements is evident through all that he does. He delivers creative vision and strategy, while also building client relationships and providing direction and support for Favorite Medium’s internal teams. Having started his career as a visual designer, Andy sees the digital medium as combining the best of print and motion with the added magic of interactivity, creating something dynamic and entirely new. </p>
                  <p>As the Creative Director of Funny Garbage, Andy’s award-winning interactive media work with global brands including the Cartoon Network, Lego, Noggin, Smithsonian Institution, The-N, Turner Broadcasting, and Wenner Media, won him a lot of attention and praise. Not one to lay idle, Andy was also an Adjunct Professor and thesis advisor at the School of Visual Arts, New York and recently co-authored a book called Interactive Design, published by Rockport Press, 2012. He also designs a line of greeting cards. </p>
                  <label for="leader.5" class="expand-details"></label>
                </div>
              </article>
            </section>
            <section id="the-team" data-js-tab-item="team" class="content">
              <p class="visuallyhidden">With , Favorite Medium team members includes .
              </p>
              <p class="visuallyhidden">
                Below are the team members' profile protos
                
                
              </p>
              <section class="members">
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306752579-junrong.png" alt="Jun Rong Tan&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Jun Rong Tan
                      </h4>
                      <h5 class="role">The Athlete
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Jun Rong Tan.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306847060-anders.png" alt="Anders McCarthy&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Anders McCarthy
                      </h4>
                      <h5 class="role">Viola Shredder
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Anders McCarthy.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306783735-michelle.png" alt="Michelle Tan&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Michelle Tan
                      </h4>
                      <h5 class="role">Number Cruncher
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Michelle Tan.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407305827518-asri.png" alt="Asri Jaffar&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Asri Jaffar
                      </h4>
                      <h5 class="role">Health Nut
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Asri Jaffar.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306879242-andy.png" alt="Andy Hwang&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Andy Hwang
                      </h4>
                      <h5 class="role">The New Guy
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Andy Hwang.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407307900518-efren.png" alt="Efren Lim&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Efren Lim
                      </h4>
                      <h5 class="role">Archer
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Efren Lim.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1418182169116-1416949233604-McEvoy-Pecko_Headshot.jpg" alt="Fionnuala&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Fionnuala
                      </h4>
                      <h5 class="role">Cat Herder
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Fionnuala.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1404914848608-joel.png" alt="Joel Carlos&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Joel Carlos
                      </h4>
                      <h5 class="role">MTB Junkie
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Joel Carlos.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306829839-jon.png" alt="Jon Siegel&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Jon Siegel
                      </h4>
                      <h5 class="role">Kopi Uncle
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Jon Siegel.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306904528-naree.png" alt="Naree Kang&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Naree Kang
                      </h4>
                      <h5 class="role">Zebra Lover
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Naree Kang.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306732405-paul.png" alt="Paul Ryan&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Paul Ryan
                      </h4>
                      <h5 class="role">Singer
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Paul Ryan.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306926003-sean.png" alt="Sean Kang&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Sean Kang
                      </h4>
                      <h5 class="role">The Custodian
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Sean Kang.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407307981434-sooyoung.png" alt="Sooyoung Park&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Sooyoung Park
                      </h4>
                      <h5 class="role">Chief Bill Payer
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Sooyoung Park.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
                <figure><img src="//s3-ap-southeast-1.amazonaws.com/fm-home-images/1407306954911-ziming.png" alt="Ziming Hu&amp;rsquo;s profile photo">
                  <figcaption class="profile">
                    <header>
                      <h4 class="name">Ziming Hu
                      </h4>
                      <h5 class="role">Trekker
                      </h5>
                    </header>
                  </figcaption>
                  <!--<img src="/images/team/Ziming Hu.png" alt="undefined&amp;rsquo;s profile photo">
                  -->
                  <!-- figcaption.profile
                  <header>
                    <h4  class="name"></h4>
                    <h5  class="role"></h5>
                  </header>
                  -->
                </figure>
              </section>
              <section>
                <div class="bubbles">
                  <div class="bubble pop popout">
                    <div style="background-image: url('//s3-ap-southeast-1.amazonaws.com/fm-home-images/1382594135978-pocket-lint.jpg');" class="bubble-bg"></div>
                    <div class="bubble-icon-press"></div>
                    <div class="bubble-info">
                      <div class="bubble-foot"><span>╳</span>
                        <h4>Pocket-lint</h4>
                        <p>The ultimate chart to what’s being discussed on social networks</p>
                      </div>
                    </div>
                  </div>
                  <div class="bubble pop popout">
                    <div style="background-image: url('//s3-ap-southeast-1.amazonaws.com/fm-home-images/1382594135978-pocket-lint.jpg');" class="bubble-bg"></div>
                    <div class="bubble-icon-press"></div>
                    <div class="bubble-info">
                      <div class="bubble-foot"><span>╳</span>
                        <h4>Pocket-lint</h4>
                        <p>The ultimate chart to what’s being discussed on social networks</p>
                      </div>
                    </div>
                  </div>
                  <div class="bubble pop popout">
                    <div style="background-image: url('//s3-ap-southeast-1.amazonaws.com/fm-home-images/1382594135978-pocket-lint.jpg');" class="bubble-bg"></div>
                    <div class="bubble-icon-press"></div>
                    <div class="bubble-info">
                      <div class="bubble-foot"><span>╳</span>
                        <h4>Pocket-lint</h4>
                        <p>The ultimate chart to what’s being discussed on social networks</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </section>
          </section>
        </div>
      </div>
    </section>
<?php get_footer(); ?>