<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,600,300">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/stylesheets/index.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/stylesheets/editor.css">
     <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
    <script>

    </script>
  </head>
    <?php 
      if(is_singular() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');
        wp_head();
    ?>
  <body>
    <nav id="main-menu" role="complementary"><a href="javascript:void(0)" class="close big">&#9587;<span class="visuallyhidden">Close Menu</span></a>
      <section>
        <?php menu_attr(); ?>
      </section>
      <section class="menu-section">
        <a href="/work.html" data-edit-key="misc.mainNav.work" class="menu-item">Featured Work</a>
        <ul class="menu">
          <li><a href="/case-study/intellian.html" class="menu-item">Intellian</a></li>
        </ul>
      </section>
      <section>
        <span class="visuallyhidden"></span>
        <a href="http://www.linkedin.com/company/favorite-medium" class="circle gray icon linked-in">
          <span class="visuallyhidden"> Linked-In</span>
        </a>
        <span class="visuallyhidden"> ,</span>
        <a href="https://twitter.com/favoritemediums" class="circle gray icon twitter">
          <span class="visuallyhidden"> Twitter</span>
        </a>
        <span class="visuallyhidden"> ,</span>
        <a href="https://github.com/favoritemedium" class="circle gray icon github">
          <span class="visuallyhidden"> Gitbub</span>
        </a>
        <span class="visuallyhidden"> , and</span>
        <a href="https://www.facebook.com/favoritemedium" class="circle gray icon facebook">
          <span class="visuallyhidden"> Facebook</span>
        </a>
        <p class="languages white hide">
          <a href="<?php echo home_url();?>/contact-us/" class="active">English</a>
          <a href="<?php echo home_url();?>/contact-us/">한국어  </a><!-- korean -->
          <a href="<?php echo home_url();?>/contact-us/">简体中文</a><!-- chinese -->
          <a href="<?php echo home_url();?>/contact-us/">Japanese</a></p>
      </section>
    </nav>

    <header id="site-head" data-js-follow>
      <div>
        <a title="toggle sidebar" data-js-sidebar href="javascript:void(0)" class="main-menu left">
          <span class="menu white">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </span>
          <span class="visuallyhidden">Main Menu</span>
        </a>
        <a href="<?php echo home_url();?>" class="logo small hide-for-inline-small left">
          <small class="visuallyhidden">Link to homepage</small></a>
          <a data-js-sidebar href="javascript:void(0)" class="logo small show-for-inline-small"></a>
        <div class="right">
          <h3 class="lwt">
            <a href="<?php echo home_url();?>/contact-us/" data-edit-key="misc.workTogether">Let's work together</a>
          </h3>
        </div>
      </div>
    </header>
