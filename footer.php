	
	<footer id="site-foot">
	  <section class="row activities">
	    <div class="columns large-6 pr50">
	      <div class="row space2"><span class="left"><strong>What we're thinking</strong></span><span class="right hide"><strong><a data-js-modal="blog">VIEW ALL</a></strong></span></div>
	      <ul id="activities" class="image-content text-left">
	        <?php
						$wp_query = new WP_Query();
						$wp_query->query(array('post_type' => 'left-activity', 'posts_per_page' => 3));
						
						if ($wp_query->have_posts()){ 
							$wp_query-> get_posts();
							while ($wp_query->have_posts()){
								$wp_query->the_post(); 

								$author_image = types_render_field("author-image", array('raw'=>'true', 'url' => 'true'));
								$activity_link = types_render_field("activity-link", array('raw'=>'true', 'url' => 'true'));
								$title = types_render_field("title", array('raw' => 'true'));
								$description = types_render_field("description", array('raw' => 'true'));
								$author = types_render_field("author", array('raw' => 'true'));
				        
				        echo '<li>';
				        	 echo '<img src="'.$author_image.'">';
				           echo '<div class="text-content">';
				            echo '<h4><a href="'.$activity_link.'">'.$title.'</a></h4>';
				            echo '<p class="description">'.$description.'</p>';
				            echo '<div class="author">'.$author.'</div>';
				          echo '</div>';
				        echo '</li>';
	      			} 
	      		}
	      		wp_reset_query();
	      	?>
	      </ul>
	    </div>
	    <div class="columns large-6 pr50">
	      <div class="row space2"><span class="left"><strong>What we're up to</strong></span><span class="right hide"><strong><a data-js-modal="activities">VIEW ALL</a></strong></span></div>
	      <ul id="activities" class="image-content text-left">
	        <?php
						$query = new WP_Query();
						$query->query(array('post_type' => 'right-activity','posts_per_page' => 3));
						
						if ($query->have_posts()){ 
							$query-> get_posts();
							while ($query->have_posts()){
								
								$query->the_post(); 
								$site_logo = types_render_field("site-logo", array('raw'=>'true', 'url' => 'true'));
								$activity_title = types_render_field("activity-title", array('raw'=>'true'));
								$activity_link_right = types_render_field("activity-link-right", array('raw'=>'true', 'url' => 'true'));
								$activity_description = types_render_field("activity-description", array('raw'=>'true'));
				
				        echo '<li>';
				        	echo '<img src="'.$site_logo.'">';
				          echo '<div class="text-content">';
				            echo '<h4><a href="'.$activity_link_right.'">'.$activity_title.'</a></h4>';
				            echo '<p class="description">'.$activity_description.'</p>';
				          echo '</div>';
				        echo '</li>';

			      	}
			    	}
			    	wp_reset_query();
	        ?>
	      </ul>
	    </div>
	  </section>
	  <hr class="space1">
	  <div class="row">
	    <div class="column large-6">
	    	<a href="/" title="Link to homepage" class="logo footer"></a>
	      <div class="text-content text-left"><a href="/contact-us.html" class="footer">
	      	<span class="visuallyhidden"> Send us your thought at our contact us page to our team at</span><span data-edit-key="misc.footerOffice.locations"> Singapore / San Francisco / Seoul</span><span class="visuallyhidden"> offices</span></a>
	        <p class="copyright">
	        	<span data-edit-key="misc.copyrights">Copyright © Favorite Medium. All Rights Reserved.</span>
	        	<small>
	        		<span data-edit-key="misc.photos.start">All photos by</span>
	        		<a href="http://www.flickr.com/photos/jonsiegel" target="_BLANK" class="footer"> Jon Siegel</a><span data-edit-key="misc.photos.end"> unless otherwise noted.</span></small></p>
	      </div>
	    </div>
	    <div class="column large-6">
	      <p class="text-right"><span class="visuallyhidden">You can find us on</span>
	      	<a href="http://www.linkedin.com/company/favorite-medium" class="circle icon linked-in">
	      		<span class="visuallyhidden"> Linked-In</span>
	      	</a>
	      		<span class="visuallyhidden"> ,</span>
	      	<a href="https://twitter.com/favoritemediums" class="circle icon twitter">
	      		<span class="visuallyhidden"> Twitter</span>
	      	</a>
	      		<span class="visuallyhidden"> ,</span>
	      	<a href="https://github.com/favoritemedium" class="circle icon github">
	      		<span class="visuallyhidden"> Gitbub</span>
	      	</a><span class="visuallyhidden"> , and</span>
	      	<a href="https://www.facebook.com/favoritemedium" class="circle icon facebook">
	      		<span class="visuallyhidden"> Facebook</span>
	      	</a>
	      </p>
	      <p class="languages hidden">
	      	<a href="/" class="active">English</a>
	      	<a href="/zh/">简体中文bn</a>
	      </p>
	    </div>
	  </div>
	</footer>
	
	<!--Code For admin toolbar that will show when admin or user login. -->
	<?php wp_footer(); ?>
	
	</body>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/scripts/jquery-2.1.0.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/scripts/CSSPlugin.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/scripts/TweenLite.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/javascripts/sidebar.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/javascripts/main.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/scripts/app.min.js"></script>
</html>

