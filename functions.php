<?php
	if(function_exists('register_sidebar')){
		register_sidebar(array(
			'name' => 'Default Widget Area',
			'id' => 'default-widget-area',
			'before_widget' => '<li class="widget-content">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'));
		register_sidebar(array(
			'name' => 'Right Widget Area',
			'id' => 'widget-area-right',
			'before_widget' => '<li class="widget-content">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'));
		register_sidebar(array(
			'name' => 'Footer Widget Area',
			'id' => 'widget-area-footer',
			'before_widget' => '<div class="widget-content col-md-3">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'));
	}

	register_nav_menus( array(
	    'primary' => __( 'Primary Menu', 'wordpress-theme' ),
	) );

	//adding favicon on front-end and admin
	function add_my_favicon() {
	   $favicon_path = get_template_directory_uri() . '/assets/images/favicons/icon-16.png';

	   echo '<link rel="shortcut icon" href="' . $favicon_path . '" />';
	}

	add_action( 'wp_head', 'add_my_favicon' ); //activating in front end
	add_action( 'admin_head', 'add_my_favicon' );//activating in admin

	require_once('wp_bootstrap_navwalker.php');

	//menu code for created menu, class and id's are changable here
	function menu_attr(){  
		$cleanermenu = wp_nav_menu( array(
                'menu'              => 'primary',
                'container'			=> false,
                'echo'				=> true,
                'items_wrap'        => '%3$s',
                'theme_location'    => 'primary',
                'depth'             => 0,
                'menu_class'        => 'menu-item',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
	}

	function remove_ul ( $menu ){
    	return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
	}

	add_filter( 'wp_nav_menu', 'remove_ul' );

	function remove_li ( $menu ){
    	return preg_replace( array( '@\<li([^>]*)>(.*?)@i' ), '', $menu, -1 );
	}

	function adding_css(){
		wp_register_style('css-style', get_template_directory_uri() . '/style.css');
	    wp_enqueue_style('css-style');
	}

	add_action( 'init', 'adding_css' );

	add_filter( 'wp_nav_menu', 'remove_li' );

	function add_nav_class($output) {
		$output= preg_replace('/<a/', '<a class="menu-item"', $output, -1);
		return $output;
	}
	
	add_filter('wp_nav_menu', 'add_nav_class');

	//code for activating post/page featured image
	add_theme_support('post-thumbnails');

	function custom_edd_add_custom_field_support( $supports ) {
		$new = array( 'custom-fields' );
		return array_merge( $new, $supports );
	}

?>